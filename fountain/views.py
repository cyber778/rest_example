import urlparse, random
from django.contrib.auth.models import User, Group
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from serializers import UserSerializer, GroupSerializer
from django.views.decorators.csrf import csrf_exempt



# Generate Cities
CITIES = []

for i in range(0,150):
    data ={
        "value": "{0}-city-{0}".format(i),
        "data": "{0}-city-{0}".format(i)
    }
    CITIES.append(data)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


@csrf_exempt
def user_list(request):
    """
    List all code users, or create a new user.
    """
    if request.method == 'GET':
        users = User.objects.all()
        serializer = UserSerializer(users, many=True, context={"request":request})
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def user_detail(request, pk):
    """
    Retrieve, update or delete a user.
    Using PUT I had to do a little work around...
    """
    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data =  dict(urlparse.parse_qsl(urlparse.urlsplit("?" + request.body).query))
        serializer = UserSerializer(user, data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        user.delete()
        return HttpResponse(status=204)


def home(request):
    """
    render main template with 3 examples
    :param request:
    :return:
    """
    ctx = {}
    return render(request, "form.html", ctx)


def city_suggest(request):
    """
    Used
    :param request: "query" must be in the object
    :return: list of filtered dictionaries
    """
    query = request.GET["query"]
    filtered_cities = filter(lambda city: query in city['data'], CITIES)[:10]
    return JsonResponse({"suggestions":filtered_cities})