/**
 * Created by user on 21/09/2016.
 */
// Just for POC used jquery
$(function(){

    $('input').change(function(){
        var elem = $(this);
        elem.closest('.input').toggleClass('input-filled', elem.val().length>0);
    });

    $('.btn').click(function(){
        //resetForm()
        var elem = $(this);
        var form = elem.closest('form');
        var validate_type = elem.data('validate');
        var url = elem.data('url');
        var method = form.attr("method");
        var data = {};
        $.each(form.find('input'), function(){
            var inner_elem = $(this);
            var name = inner_elem.attr("name");
            data[name] = inner_elem.val();
        });

        console.log(data);
        if(!form.valid()){
            form.find('.input').addClass('input-filled');
            return false
        }

        else if(method=="GET") {
            $.get(url, data).done(function (data) {
                var modal = $('#userModal');
                var m_modal = modal.find("#modalBody");
                var html = "";
                $.each(data, function(){
                    html += "<ul>";
                    inner_data = this;
                    $.each(inner_data, function(key, val){
                        if (key=="url") return;
                        html += "<li>"+ "<strong>" + key + ": </strong>" + val + "</li>"
                    });
                    html += "</ul><hr/>";
                });
                m_modal.html(html);
                modal.modal("show");
                console.log(data);
            });
        }

        else if(method=="PUT"){
            $.ajax({
              url: url + form.find("[name='pk']").val() + '/',
              type: 'PUT',
              data: data,
              success: function(data) {
                alert('Load was performed.');
              }
            });
        }

        // Not used currently
        else if (method=="POST"){
            $.post(url+ form.find("[name='pk']").val() + '/', data).done(function (data) {
                console.log(data);
            });
        }


    });


    // Autocomplete

    $('input[name="city"]').autocomplete({
        serviceUrl: '/api/city-suggest/'
    });

    // INIT validators

    $('#autocomplete').validate({
        rules: {
            city: {
                required: true,
                letters: true
            }
        }
    });

    $('#edit-user').validate({
        rules: {
            pk:{
                required: true,
                number: true
            },
            email: {
              required: true,
              email: true
            }
      }
    })

});