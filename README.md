# GrowthFountain Task
![N|Solid](http://growthfountain.com/images/gf_icon.png)

I have completed 4 tasks regarding REST API:

  1) created a GUI to edit users at ```/map``` (Rest framework)

  2) get all users at the home page (Rest framework)

  3) edit user email by pk and username (Rest framework)
  
  4) autocomplete POC (Self built API)

  * The views are already built for more functions such as delete...

### Requirments
  - Django 1.9 (only tested on this version but probrbly will work with older ones too)
  - djangorestframework 3.4.6
  - Python 2.7

### Models & Superuser

Since I am using sqlite the file is already commited and there is no need to do any migrations.

To login with superuser use "admin" as username and "pronto12" as password


### Client Side

In order not to delay with single page app config files and setup I used Jquery with bootstrap 3
and some 3rd party plugins taken from cdnjs


### Todo

- add scss

- more options

- add js framework and do everything more dynamic

- add tests

